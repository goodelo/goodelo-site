import { useQuery, useMutation } from '@apollo/client';

import { isEmptyUser } from 'utils/users';
import { AUTHED_CLIENT_NAME } from 'state/store';
import { GET_CURRENT_USER } from 'state/users';

export const useAuthQuery = (query, options) => {
  const {
    data: { currentUser },
  } = useQuery(GET_CURRENT_USER);

  return useQuery(query, {
    ...options,
    skip: isEmptyUser(currentUser),
    context: { clientName: AUTHED_CLIENT_NAME },
  });
};

export const useAuthMutation = (mutation, options) => {
  return useMutation(mutation, {
    ...options,
    context: { clientName: AUTHED_CLIENT_NAME },
  });
};
