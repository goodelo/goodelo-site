import { path } from 'ramda';
import { useQuery } from '@apollo/client';

import { GET_CURRENT_USER } from 'state/users';

export function useCurrentUser() {
  const { data } = useQuery(GET_CURRENT_USER);
  return path(['currentUser'], data);
}
