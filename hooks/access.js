import { useQuery } from '@apollo/client';

import { ROLE } from 'constants/users';
import { GET_CURRENT_USER } from 'state/users';
import { isEmptyUser } from 'utils/users';

const ROLE_LEVELS = {
  [ROLE.ANONIM]: 0,
  [ROLE.REGULAR]: 1,
  [ROLE.MODERATOR]: 2,
  [ROLE.ADMIN]: 3,
};

export const useHasAccess = role => {
  const {
    data: { currentUser },
  } = useQuery(GET_CURRENT_USER);

  return (
    !isEmptyUser(currentUser) &&
    ROLE_LEVELS[currentUser.role] >= ROLE_LEVELS[role]
  );
};
