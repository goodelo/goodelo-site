export const decodeAttachment = attachmentString => {
  try {
    return JSON.parse(attachmentString);
  } catch (e) {
    return [];
  }
};

export const encodeAttachment = attachmentArray => {
  return JSON.stringify(attachmentArray);
};

export const validateGoodelo = goodelo => goodelo.title && goodelo.description;
