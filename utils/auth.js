import { Auth } from 'aws-amplify';

export const signIn = async (username, password) =>
  await Auth.signIn(username, password);

export const signOut = async () => await Auth.signOut();

export const signUp = async ({ username, password, email, role }) =>
  await Auth.signUp({
    username,
    password,
    attributes: {
      email,
      'custom:role': role,
    },
  });

export const confirmSignUp = async (username, code) =>
  await Auth.confirmSignUp(username, code);

export const forgotPassword = async username =>
  await Auth.forgotPassword(username);

export const forgotPasswordSubmit = async (username, code, newPassword) =>
  await Auth.forgotPasswordSubmit(username, code, newPassword);

export const getCurrentSession = async () => await Auth.currentSession();

export const getJwtToken = async () =>
  (await getCurrentSession()).getIdToken().getJwtToken();

export const getCurrentAuthUser = async options =>
  await Auth.currentAuthenticatedUser(options);

export const getCurrentCredentials = async () =>
  await Auth.currentCredentials();

export const updateUserAttributes = async (user, attributes) =>
  await Auth.updateUserAttributes(user, attributes);

export const verifyCurrentUserAttributeSubmit = async (attrName, code) =>
  await Auth.verifyCurrentUserAttributeSubmit(attrName, code);

export const refreshSession = async () => {
  const session = await getCurrentSession();

  return await refreshSessionPromise(session.getRefreshToken());
};

const refreshSessionPromise = async refreshToken =>
  new Promise(async (resolve, reject) => {
    const user = await getCurrentAuthUser();
    return user.refreshSession(refreshToken, async (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data); // THIS IS YOUR REFRESHED ATTRIBUTES/GROUPS
      }
    });
  });
