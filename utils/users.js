import { pipe, path, isNil } from 'ramda';

export const isEmptyUser = pipe(path(['username']), isNil);
