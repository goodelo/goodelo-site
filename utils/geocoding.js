import {
  MAP_QUEST_API_KEY,
  MAP_QUEST_ADDRESS_GEOCODING_URL,
} from 'constants/config';

export const geocodeAddress = async address => {
  const data = await fetch(
    `${MAP_QUEST_ADDRESS_GEOCODING_URL}?key=${MAP_QUEST_API_KEY}&location=${address.trim()}`
  );

  const {
    results: [
      {
        locations: [
          {
            latLng: { lat, lng },
            geocodeQuality,
          },
        ],
      },
    ],
  } = await data.json();

  return { lat, lng, geocodeQuality };
};
