import { Storage } from 'aws-amplify';

export const s3Upload = async (file, { onProgress }) => {
  const filename = `${Date.now()}__${file.name}`;

  const stored = await Storage.put(filename, file, {
    level: 'protected',
    contentType: file.type,
    progressCallback(progress) {
      onProgress(progress.loaded / progress.total);
    },
  });

  return stored.key;
};
