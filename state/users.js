import gql from 'graphql-tag';
import { makeVar } from '@apollo/client';

import { fragments as goodelosFragments } from 'state/goodelos';
import { DEFAULT_USER } from 'constants/users';

export const typeDefs = gql`
  extend type Query {
    currentUser: User
  }

  extend type Mutation {
    setCurrentUser(user: User!): User
    resetCurrentUser: Reponse!
  }
`;

export const fragments = {
  user: gql`
    fragment UserFields on User {
      __typename
      username
      email
      role
    }
  `,
};

export const GET_USER = gql`
  query GetUser($username: String!) {
    user(username: $username) {
      user {
        ...UserFields
      }
      goodelos {
        ...GoodeloFields
      }
    }
  }

  ${fragments.user}
  ${goodelosFragments.goodelo}
`;

export const GET_CURRENT_USER = gql`
  query GetCurrentUser {
    currentUser @client {
      ...UserFields
    }
  }

  ${fragments.user}
`;

export const currentUserVar = makeVar(DEFAULT_USER);
