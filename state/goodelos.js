import gql from 'graphql-tag';

export const typeDefs = gql`
  extend type Mutation {
    createGoodelo(input: GoodeloInput!): Goodelo!
  }
`;

export const fragments = {
  goodelo: gql`
    fragment GoodeloFields on Goodelo {
      __typename
      id
      username
      createdAt
      updatedAt
      identityId

      title
      description
      attachment
      lat
      lng
    }
  `,
  cursor: gql`
    fragment CursorFields on Cursor {
      __typename
      id
      partitionId
      createdAt
    }
  `,
};

export const GET_GOODELOS = gql`
  query GetGoodelos {
    goodelos {
      items {
        ...GoodeloFields
      }
      nextPageCursor {
        ...CursorFields
      }
    }
  }

  ${fragments.goodelo}
  ${fragments.cursor}
`;

export const GET_MAP_GOODELOS = gql`
  query GetAllGoodelos {
    goodelosAll {
      items {
        ...GoodeloFields
      }
    }
  }

  ${fragments.goodelo}
`;

export const GET_MORE_GOODELOS = gql`
  query GetMoreGoodelos($cursor: CursorInput) {
    goodelos(cursor: $cursor) {
      items {
        ...GoodeloFields
      }
      nextPageCursor {
        ...CursorFields
      }
    }
  }

  ${fragments.goodelo}
  ${fragments.cursor}
`;

export const GET_GOODELO = gql`
  query GetGoodelo($id: String!) {
    goodelo(id: $id) {
      ...GoodeloFields
    }
  }

  ${fragments.goodelo}
`;

export const CREATE_GOODELO = gql`
  mutation createGoodelo($input: GoodeloInput!) {
    createGoodelo(input: $input) {
      ...GoodeloFields
    }
  }

  ${fragments.goodelo}
`;

export const UPDATE_GOODELO = gql`
  mutation updateGoodelo($id: String!, $input: GoodeloInput!) {
    updateGoodelo(id: $id, input: $input) {
      ...GoodeloFields
    }
  }

  ${fragments.goodelo}
`;
