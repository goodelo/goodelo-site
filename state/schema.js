import gql from 'graphql-tag';

import { typeDefs as currentUserTypeDefs } from 'state/users';
import { typeDefs as goodelosTypeDefs } from 'state/goodelos';

const typeDefs = gql`
  ${currentUserTypeDefs}
  ${goodelosTypeDefs}
`;

export default typeDefs;
