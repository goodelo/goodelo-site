import {
  ApolloClient,
  InMemoryCache,
  createHttpLink,
  ApolloProvider,
  ApolloLink,
} from '@apollo/client';
import { assocPath } from 'ramda';
import fetch from 'isomorphic-fetch';
import withApollo from 'next-with-apollo';
import { getDataFromTree } from '@apollo/react-ssr';

import resolvers from 'state/resolvers';
import typeDefs from 'state/schema';
import {
  GOODELO_CORE_URL,
  IS_SERVER_SIDE,
  IS_DEVELOPMENT,
} from 'constants/config';
import { getJwtToken } from 'utils/auth';
import { currentUserVar } from 'state/users';

const link = createHttpLink({
  uri: `${GOODELO_CORE_URL}/graphql-open`,
  fetch,
});

const authedLink = createHttpLink({
  uri: `${GOODELO_CORE_URL}/graphql`,
  fetch: async (uri, options) => {
    try {
      const token = await getJwtToken();

      return fetch(
        uri,
        assocPath(['headers', 'authorization'], token, options)
      );
    } catch (e) {
      // no current user
      return fetch(uri, options);
    }
  },
  credentials: 'omit',
});

export const AUTHED_CLIENT_NAME = 'authed';

const create = () => {
  const cacheConfig = {
    typePolicies: {
      Query: {
        fields: {
          currentUser: {
            read() {
              return currentUserVar();
            },
          },
        },
      },
    },
  };

  // Check out https://github.com/zeit/next.js/pull/4611 if you want to use the AWSAppSyncClient
  const client = new ApolloClient({
    connectToDevTools: !IS_SERVER_SIDE,
    ssrMode: !IS_DEVELOPMENT && IS_SERVER_SIDE, // Disables forceFetch on the server (so queries are only run once)
    link: ApolloLink.split(
      ({ getContext }) => getContext().clientName === AUTHED_CLIENT_NAME,
      authedLink,
      link
    ),
    cache: new InMemoryCache(cacheConfig),
    typeDefs,
    resolvers,
  });

  return client;
};

export let apolloClient = create();

const initApollo = () => {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (IS_SERVER_SIDE) {
    return create();
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = create();
  }

  return apolloClient;
};

export const withStore = withApollo(initApollo, {
  render: ({ Page, props }) => (
    <ApolloProvider client={props.apollo}>
      <Page {...props} />
    </ApolloProvider>
  ),
  getDataFromTree,
});
