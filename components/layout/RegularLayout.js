import React from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import styled from '@emotion/styled';

import Layout from 'components/layout/Layout';

const ColumnContainer = ({ children, ...props }) => (
  <StyledContainer maxWidth='sm' {...props}>
    <Box display='flex' flexDirection='column' p={2}>
      {children}
    </Box>
  </StyledContainer>
);

const StyledContainer = styled(Container)`
  background-color: ${({ theme }) => theme.palette.background.paper};
  height: 100%;
  flex: 1;
`;

const RegularLayout = props => (
  <Layout {...props} contentWrapper={ColumnContainer} />
);

export default RegularLayout;
