import React from 'react';
import styled from '@emotion/styled';

import Layout from 'components/layout/Layout';

const FullPageLayout = props => (
  <Layout {...props} contentWrapper={PageContent} />
);

const PageContent = styled.div`
  flex: 1 1 0;
  display: flex;
  flex-direction: column;
`;

export default FullPageLayout;
