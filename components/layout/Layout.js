import React, { useEffect } from 'react';
import styled from '@emotion/styled';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import { useQuery } from '@apollo/client';
import { path } from 'ramda';

import { GET_CURRENT_USER } from 'state/users';
import Header from 'components/layout/header/Header';
import { isEmptyUser } from 'utils/users';
import { getCurrentAuthUser } from 'utils/auth';
import { ROLE, DEFAULT_USER } from 'constants/users';
import { currentUserVar } from 'state/users';
import { useCurrentUser } from 'hooks/users';

const Layout = ({
  children,
  className,
  contentWrapper: ContentWrapper = Content,
  title,
}) => {
  const currentUser = useCurrentUser();

  useEffect(() => {
    const getCurrentUser = async () => {
      try {
        const user = await getCurrentAuthUser({
          bypassCache: true, // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
        });

        currentUserVar({
          __typename: 'User',
          username: user.username,
          email: user.attributes['email'],
          role: user.attributes['custom:role'] || ROLE.REGULAR,
        });
      } catch (e) {
        currentUserVar(DEFAULT_USER);
      }
    };

    if (isEmptyUser(currentUser)) {
      getCurrentUser();
    }
  }, [currentUser]);

  return (
    <Container
      display='flex'
      height='100%'
      flexDirection='column'
      className={className}
    >
      <CssBaseline />
      <Header title={title} />
      <ContentWrapper className='page-content'>{children}</ContentWrapper>
    </Container>
  );
};

const Container = styled(Box)`
  background-color: ${({ theme }) => theme.palette.background.default};
  flex: 1;
`;

const Content = styled.div`
  flex: 1 1 0;
`;

export default Layout;
