import React, { useState } from 'react';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import styled from '@emotion/styled';
import ListItem from '@material-ui/core/ListItem';
import { useIntl } from 'react-intl';
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';
import HomeIcon from '@material-ui/icons/Home';
import { useRouter } from 'next/router';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RoomIcon from '@material-ui/icons/Room';

import { ROUTES } from 'constants/router';
import { DEFAULT_USER } from 'constants/users';
import { signOut } from 'utils/auth';
import { currentUserVar } from 'state/users';

const SideMenu = ({ currentUser }) => {
  const [isOpen, setIsOpen] = useState(false);
  const intl = useIntl();
  const router = useRouter();

  const onLogoutClick = async () => {
    await signOut();
    currentUserVar(DEFAULT_USER);
  };

  const menu = (
    <Menu onClick={() => setIsOpen(false)} onKeyDown={() => setIsOpen(false)}>
      <List>
        <ListItem button onClick={() => router.push(ROUTES.MAIN)}>
          <ListItemIcon>
            <HomeIcon />
          </ListItemIcon>
          <ListItemText
            primary={intl.formatMessage({ id: 'layout.menu.home' })}
          />
        </ListItem>
        <ListItem button onClick={() => router.push(ROUTES.MAP)}>
          <ListItemIcon>
            <RoomIcon />
          </ListItemIcon>
          <ListItemText
            primary={intl.formatMessage({ id: 'layout.menu.map' })}
          />
        </ListItem>
        <ListItem
          button
          onClick={() => router.push(`${ROUTES.USER}/${currentUser.username}`)}
        >
          <ListItemIcon>
            <AccountCircleIcon />
          </ListItemIcon>
          <ListItemText
            primary={intl.formatMessage({ id: 'layout.menu.profile' })}
          />
        </ListItem>
        <ListItem button onClick={() => router.push(ROUTES.ADD_GOODELO)}>
          <ListItemIcon>
            <AddCircleIcon />
          </ListItemIcon>
          <ListItemText
            primary={intl.formatMessage({ id: 'layout.menu.addGoodelo' })}
          />
        </ListItem>
      </List>
      <Divider />
      <List>
        <ListItem button onClick={onLogoutClick}>
          <ListItemIcon>
            <MeetingRoomIcon />
          </ListItemIcon>
          <ListItemText primary={intl.formatMessage({ id: 'layout.logout' })} />
        </ListItem>
      </List>
    </Menu>
  );

  return (
    <>
      <Box display='flex' alignItems='center' mr='1rem'>
        <IconButton
          edge='start'
          color='inherit'
          aria-label='menu'
          onClick={() => setIsOpen(!isOpen)}
        >
          <MenuIcon />
        </IconButton>
      </Box>
      <SwipeableDrawer
        anchor='left'
        open={isOpen}
        onClose={() => setIsOpen(false)}
        onOpen={() => setIsOpen(true)}
      >
        {menu}
      </SwipeableDrawer>
    </>
  );
};

const Menu = styled.div`
  width: 18rem;
`;

export default SideMenu;
