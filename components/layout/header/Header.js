import React from 'react';
import { useRouter } from 'next/router';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import styled from '@emotion/styled';
import { useQuery } from '@apollo/client';
import { FormattedMessage } from 'react-intl';
import { path } from 'ramda';

import { ROUTES } from 'constants/router';
import { isEmptyUser } from 'utils/users';
import Button from 'components/common/Button';
import Link from 'components/common/Link';
import SideMenu from 'components/layout/header/SideMenu';
import { ReactComponent as Logo } from 'assets/images/logo.svg';
import { GET_CURRENT_USER } from 'state/users';

const Header = ({ title }) => {
  const { data } = useQuery(GET_CURRENT_USER);
  const currentUser = path(['currentUser'], data);
  const router = useRouter();

  return (
    <>
      <HideOnScroll>
        <AppBar>
          <Toolbar>
            <Box
              display='flex'
              alignItems='stretch'
              flex='1'
              justifyContent='space-between'
            >
              <Box display='flex' alignItems='center'>
                {!isEmptyUser(currentUser) && (
                  <SideMenu currentUser={currentUser} />
                )}

                <Box display='flex' alignItems='center'>
                  <Box mr='1rem'>
                    <StyledLink href={ROUTES.MAIN}>
                      <StyledLogo />
                    </StyledLink>
                  </Box>
                  <StyledH2>{title}</StyledH2>
                </Box>
              </Box>
              {isEmptyUser(currentUser) ? (
                <Button
                  variant='text'
                  onClick={() => router.push(ROUTES.LOGIN)}
                >
                  <FormattedMessage id='layout.login' />
                </Button>
              ) : (
                <IconButton
                  aria-label='account of current user'
                  aria-controls='menu-appbar'
                  aria-haspopup='true'
                  onClick={e =>
                    router.push(`${ROUTES.USER}/${currentUser.username}`)
                  }
                  color='inherit'
                >
                  <AccountCircle />
                </IconButton>
              )}
            </Box>
          </Toolbar>
        </AppBar>
      </HideOnScroll>
      <Toolbar />
    </>
  );
};

function HideOnScroll({ children }) {
  const trigger = useScrollTrigger();

  return (
    <Slide appear={false} direction='down' in={!trigger}>
      {children}
    </Slide>
  );
}

const StyledLogo = styled(Logo)`
  width: 3rem;
  height: 3rem;
`;

const StyledLink = styled(Link)`
  display: flex;
  align-items: center;
`;

const StyledH2 = styled.h2`
  margin: 0;
  white-space: nowrap;
`;

export default Header;
