import React, { useEffect } from 'react';
import { useRouter } from 'next/router';

import { ROUTES } from 'constants/router';
import { getCurrentAuthUser } from 'utils/auth';
import { currentUserVar } from 'state/users';
import { DEFAULT_USER } from 'constants/users';
import { useCurrentUser } from 'hooks/users';

const privateRoute = WrappedComponent => props => {
  const router = useRouter();
  const currentUser = useCurrentUser();

  useEffect(() => {
    const checkUser = async () => {
      try {
        await getCurrentAuthUser();
      } catch (e) {
        currentUserVar(DEFAULT_USER);
        router.push(ROUTES.LOGIN);
      }
    };

    checkUser();
  }, [currentUser, router]);

  return <WrappedComponent {...props} />;
};

export default privateRoute;
