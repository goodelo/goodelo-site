import React, { useState, useEffect } from 'react';
import { useIntl, FormattedMessage } from 'react-intl';
import Box from '@material-ui/core/Box';
import styled from '@emotion/styled';

import {
  getCurrentAuthUser,
  updateUserAttributes,
  refreshSession,
  verifyCurrentUserAttributeSubmit,
} from 'utils/auth';

import TextField from 'components/common/TextField';
import Button from 'components/common/Button';
import ErrorChip from 'components/common/ErrorChip';
import SuccessChip from 'components/common/SuccessChip';
import { currentUserVar } from 'state/users';
import { ROLE } from 'constants/users';

const ChangeUserEmailForm = () => {
  const [email, setEmail] = useState('');
  const [code, setCode] = useState('');
  const [user, setUser] = useState(null);
  const [isCodeSent, setIsCodeSent] = useState(false);
  const [status, setStatus] = useState(null);
  const intl = useIntl();

  useEffect(() => {
    getCurrentAuthUser()
      .then(setUser)
      .catch(e => console.log(e));
  }, []);

  const onChangeEmailFormSubmit = async e => {
    e.preventDefault();
    setStatus(null);

    try {
      const data = await updateUserAttributes(user, { email });
      console.log(data);
      setIsCodeSent(true);
    } catch (e) {
      setStatus('error');
      console.error(e);
    }

    return false;
  };

  const onConfirmEmailCodeFormSubmit = async e => {
    e.preventDefault();
    setStatus(null);

    try {
      await verifyCurrentUserAttributeSubmit('email', code);

      await refreshSession();

      const updatedUser = await getCurrentAuthUser({
        bypassCache: true,
      });

      currentUserVar({
        __typename: 'User',
        username: updatedUser.username,
        email: updatedUser.attributes['email'],
        role: updatedUser.attributes['custom:role'] || ROLE.REGULAR,
      });

      setStatus('success');
    } catch (e) {
      setStatus('error');
      console.error(e);
    }

    return false;
  };

  if (!user) {
    return null;
  }

  return (
    <Box display='flex' flexDirection='column' width='100%'>
      {status === 'error' && (
        <Box mb={2}>
          <ErrorChip />
        </Box>
      )}
      {status === 'success' && (
        <Box mb={2}>
          <SuccessChip />
        </Box>
      )}

      {status !== 'success' &&
        (isCodeSent ? (
          <StyledForm onSubmit={onConfirmEmailCodeFormSubmit}>
            <Box flex='1' mr={2}>
              <TextField
                required
                fullWidth
                label={intl.formatMessage({
                  id: 'account.confirmEmailCodeField',
                })}
                value={code}
                onChange={e => setCode(e.target.value)}
              />
            </Box>
            <Button type='submit'>
              <FormattedMessage id='account.confirmEmailCodeButton' />
            </Button>
          </StyledForm>
        ) : (
          <StyledForm onSubmit={onChangeEmailFormSubmit}>
            <Box flex='1' mr={2}>
              <TextField
                type='email'
                required
                fullWidth
                label={intl.formatMessage({ id: 'account.newEmailField' })}
                value={email}
                onChange={e => setEmail(e.target.value)}
              />
            </Box>
            <Button type='submit'>
              <FormattedMessage id='account.changeEmailButton' />
            </Button>
          </StyledForm>
        ))}
    </Box>
  );
};

const StyledForm = styled.form`
  display: flex;
`;

export default ChangeUserEmailForm;
