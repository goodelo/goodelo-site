import React from 'react';
import styled from '@emotion/styled';
import { S3Image } from 'aws-amplify-react';
import dayjs from 'dayjs';
import Box from '@material-ui/core/Box';
import { useRouter } from 'next/router';
import { always } from 'ramda';
import { FormattedMessage } from 'react-intl';
import Typography from '@material-ui/core/Typography';

import { ROUTES } from 'constants/router';
import Link from 'components/common/Link';
import { useHasAccess } from 'hooks/access';
import { ROLE } from 'constants/users';
import Toolbar from 'components/goodelos/toolbar/Toolbar';
import { decodeAttachment } from 'utils/goodelos';

const GoodeloCard = ({
  goodelo,
  className,
  isPreview,
  onGoodeloUpdate = always(),
}) => {
  const {
    title,
    description,
    createdAt,
    username,
    attachment,
    identityId,
    id,
    lat,
    lng,
  } = goodelo;
  const router = useRouter();
  const parsedAttachment = decodeAttachment(attachment);

  return (
    <Container
      className={className}
      hasCursorPointer={!!isPreview}
      onClick={
        isPreview ? () => router.push(`${ROUTES.GOODELO}/${id}`) : always()
      }
    >
      <Box display='flex'>
        {parsedAttachment.length ? (
          <Box
            display='flex'
            flex='0 0 30%'
            p='1rem 0 1rem 1rem'
            flexDirection='column'
            alignItems='center'
          >
            <StyledS3Image
              imgKey={parsedAttachment[0]}
              level='protected'
              identityId={identityId}
            />
          </Box>
        ) : (
          <Box
            display='flex'
            flex='0 0 30%'
            height='10em'
            justifyContent='center'
            alignItems='center'
          >
            <FormattedMessage id='goodelo.noImage' />
          </Box>
        )}
        <Box display='flex' flexDirection='column' flex='0 0 70%' width='70%'>
          <Box display='flex' flex='0 0 auto'>
            <Box
              display='flex'
              flexDirection='column'
              minWidth={0}
              p='1rem'
              alignItems='flex-start'
              flex={1}
            >
              <Box title={title} maxWidth='100%'>
                {isPreview ? (
                  <StyledLink
                    href={`${ROUTES.GOODELO}/${id}`}
                    textComponent='h2'
                    textVariant='h4'
                  >
                    {title || '<title>'}
                  </StyledLink>
                ) : (
                  <Typography variant='h4' component='h2'>
                    {title || '<title>'}
                  </Typography>
                )}
              </Box>
              <Box overflow='hidden' maxHeight='10em' mt='1rem'>
                <Typography variant='body1'>{description}</Typography>
              </Box>
            </Box>
            {useHasAccess(ROLE.REGULAR) && (
              <Toolbar goodelo={goodelo} onGoodeloUpdate={onGoodeloUpdate} />
            )}
          </Box>
          {!isPreview && (
            <>
              <Box flex='0 0 auto' p='1rem'>
                <Typography variant='subtitle2'>
                  <FormattedMessage id='goodelo.createdBy' /> <b>{username}</b>{' '}
                  <FormattedMessage id='goodelo.at' />{' '}
                  <b>{dayjs(createdAt).format('DD.MM.YY HH:mm:ss')}</b>
                </Typography>
              </Box>
              {lat && lng && (
                <Box flex='0 0 auto' p='1rem'>
                  <Typography variant='body1'>
                    <FormattedMessage id='goodeloCard.locationLabel' />: {lat},{' '}
                    {lng}
                  </Typography>
                </Box>
              )}
            </>
          )}
        </Box>
      </Box>
    </Container>
  );
};

const Container = styled.div`
  position: relative;
  box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.15);

  ${({ hasCursorPointer }) =>
    hasCursorPointer &&
    `
    cursor: pointer;
    &:hover {
      box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.25);
    }
    `}
`;

const StyledLink = styled(Link)`
  flex: 1;
  text-decoration: none;
  margin: 0 1rem 1rem 0;

  .MuiTypography-root {
    max-width: 100%;
  }

  .link-text {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`;

const StyledS3Image = styled(S3Image)`
  max-width: 100%;
  max-height: 10em;
`;

export default GoodeloCard;
