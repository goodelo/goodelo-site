import React from 'react';
import Box from '@material-ui/core/Box';
import styled from '@emotion/styled';

import GoodeloCard from 'components/goodelos/GoodeloCard';

const GoodeloList = ({ goodelos, isPreview, className }) => (
  <Box display='flex' flexDirection='column' className={className}>
    {goodelos.map(goodelo => (
      <StyledGoodeloCard
        key={goodelo.id}
        goodelo={goodelo}
        isPreview={isPreview}
      />
    ))}
  </Box>
);

const StyledGoodeloCard = styled(GoodeloCard)`
  margin-bottom: 1rem;
`;

export default GoodeloList;
