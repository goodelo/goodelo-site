import React, { useState, useRef, useEffect } from 'react';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import { useIntl } from 'react-intl';
import { pick, path } from 'ramda';
import styled from '@emotion/styled';

import Dialog from 'components/common/Dialog';
import GoodeloForm from 'components/goodelos/goodelo-form/GoodeloForm';
import { useAuthMutation } from 'hooks/api';
import { UPDATE_GOODELO } from 'state/goodelos';
import { validateGoodelo } from 'utils/goodelos';
import { UPDATABLE_FIELDS } from 'constants/goodelos';

const EditGoodeloButton = ({ goodelo, onUpdate }) => {
  const intl = useIntl();
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [error, setError] = useState(null);
  const [isSubmitDisabled, setIsSubmitDisabled] = useState(true);
  const formRef = useRef();

  const [updateGoodelo, { loading }] = useAuthMutation(UPDATE_GOODELO);

  const onSubmit = async goodeloToSave => {
    setError(null);

    try {
      const data = await updateGoodelo({
        variables: {
          id: goodeloToSave.id,
          input: pick(UPDATABLE_FIELDS)(goodeloToSave),
        },
      });

      console.log('Goodelo updated:', data);
      setIsDialogOpen(false);
      onUpdate(path(['data', 'updateGoodelo'], data));
    } catch (e) {
      console.error(e);
      setError(intl.formatMessage({ id: 'editGoodelo.saveError' }));
    }

    return false;
  };

  const onLocalUpdate = newGoodelo => {
    setIsSubmitDisabled(!validateGoodelo(newGoodelo));
  };

  useEffect(
    function resetError() {
      if (isDialogOpen) {
        setError(null);
      }
    },
    [isDialogOpen]
  );

  return (
    <>
      <IconButton
        title={intl.formatMessage({ id: 'editGoodelo.editButton' })}
        aria-label='edit'
        onClick={() => setIsDialogOpen(true)}
      >
        <EditIcon />
      </IconButton>
      <Dialog
        isOpen={isDialogOpen}
        onClose={() => setIsDialogOpen(false)}
        title={intl.formatMessage(
          {
            id: 'editGoodelo.editDialogTitle',
          },
          { goodelo: goodelo.title || '<title>' }
        )}
        submitLabel={intl.formatMessage({
          id: 'editGoodelo.editDialogSubmit',
        })}
        isSubmitDisabled={isSubmitDisabled}
        onSubmit={() => {
          // needs to make event cancellable to avoid page refresh
          const event = new Event('submit', { cancelable: true });
          formRef.current.dispatchEvent(event);
        }}
        onCancel={() => setIsDialogOpen(false)}
      >
        <StyledGoodeloForm
          goodelo={goodelo}
          onSubmit={onSubmit}
          isLoading={loading}
          error={error}
          setError={setError}
          onLocalUpdate={onLocalUpdate}
          ref={formRef}
        />
      </Dialog>
    </>
  );
};

const StyledGoodeloForm = styled(GoodeloForm)``;

export default EditGoodeloButton;
