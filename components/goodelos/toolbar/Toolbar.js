import React from 'react';
import styled from '@emotion/styled';
import { useIntl } from 'react-intl';
import ReportIcon from '@material-ui/icons/Report';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import { always } from 'ramda';
import { useQuery } from '@apollo/client';

import { useHasAccess } from 'hooks/access';
import { ROLE } from 'constants/users';
import { GET_CURRENT_USER } from 'state/users';
import EditGoodeloButton from 'components/goodelos/toolbar/EditGoodeloButton';

const Toolbar = ({ goodelo, onGoodeloUpdate }) => {
  const isModerator = useHasAccess(ROLE.MODERATOR);
  const intl = useIntl();
  const {
    data: { currentUser },
  } = useQuery(GET_CURRENT_USER);

  return (
    <Container onClick={e => e.stopPropagation()}>
      {(currentUser.username === goodelo.username || isModerator) && (
        <EditGoodeloButton goodelo={goodelo} onUpdate={onGoodeloUpdate} />
      )}
      <IconButton
        title={intl.formatMessage({ id: 'goodelo.report' })}
        aria-label='report'
        onClick={always()}
      >
        <ReportIcon />
      </IconButton>
      {isModerator && (
        <IconButton
          title={intl.formatMessage({ id: 'goodelo.hide' })}
          aria-label='hide'
          onClick={always()}
        >
          <VisibilityOffIcon />
        </IconButton>
      )}
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 3rem;
  flex: 0 0 auto;
`;

export default Toolbar;
