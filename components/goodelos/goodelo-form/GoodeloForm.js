import React, {
  useState,
  useCallback,
  useEffect,
  forwardRef,
  useRef,
} from 'react';
import Box from '@material-ui/core/Box';
import { useIntl, FormattedMessage } from 'react-intl';
import { curry, always, assoc, pipe } from 'ramda';
import styled from '@emotion/styled';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';

import ErrorChip from 'components/common/ErrorChip';
import TextField from 'components/common/TextField';
import Attachment from 'components/goodelos/goodelo-form/Attachment';
import Loader from 'components/common/Loader';
import Button from 'components/common/Button';

import { DEFAULT_GOODELO } from 'constants/goodelos';
import { getCurrentCredentials } from 'utils/auth';
import { decodeAttachment, encodeAttachment } from 'utils/goodelos';
import { geocodeAddress } from 'utils/geocoding';

const GoodeloForm = ({
  goodelo = DEFAULT_GOODELO,
  onSubmit,
  buttons,
  isLoading,
  error,
  setError,
  onLocalUpdate = always(),
  formRef,
  className,
}) => {
  const intl = useIntl();
  const addressRef = useRef(null);
  const latRef = useRef(null);

  const [localGoodelo, setLocalGoodelo] = useState(goodelo);
  const [credentials, setCredentials] = useState(null);
  const [isAddressVisible, setIsAddressVisible] = useState(false);
  const [isAddressLoading, setIsAddressLoading] = useState(false);
  const [addressError, setAddressError] = useState('');
  const [address, setAddress] = useState('');

  const updateField = useCallback(
    curry((fieldName, value) => {
      setLocalGoodelo(currentGoodelo => {
        const newGoodelo = assoc(fieldName, value, currentGoodelo);
        onLocalUpdate(newGoodelo);

        return newGoodelo;
      });
    }),
    [localGoodelo]
  );

  useEffect(() => {
    getCurrentCredentials().then(setCredentials);
  }, []);

  const onAddressSubmit = async () => {
    setIsAddressLoading(true);
    setAddressError('');

    try {
      const { lat, lng, geocodeQuality } = await geocodeAddress(address);

      if (geocodeQuality === 'COUNTRY') {
        throw new Error('Bad input');
      }

      updateField('lat', lat);
      updateField('lng', lng);
      setIsAddressVisible(false);
    } catch (e) {
      setAddressError(e.message);
      if (addressRef.current) addressRef.current.focus();
    } finally {
      setIsAddressLoading(false);
    }
  };

  return (
    <form
      onSubmit={async e => {
        e.preventDefault();
        await onSubmit(localGoodelo);
      }}
      ref={formRef}
      className={className}
    >
      <Box mb={2}>
        <TextField
          required
          fullWidth
          label={intl.formatMessage({ id: 'goodeloForm.titleField' })}
          value={localGoodelo.title}
          onChange={e => updateField('title', e.target.value)}
        />
      </Box>
      <TextField
        multiline
        rows={10}
        required
        fullWidth
        label={intl.formatMessage({ id: 'goodeloForm.descriptionField' })}
        value={localGoodelo.description}
        onChange={e => updateField('description', e.target.value)}
      />
      <Box my={2}>
        <Attachment
          fileKeyList={decodeAttachment(localGoodelo.attachment)}
          onChange={pipe(encodeAttachment, updateField('attachment'))}
          onError={setError}
          identityId={
            goodelo.identityId || (credentials && credentials.identityId)
          }
        />
      </Box>
      <FormControl component='fieldset'>
        <LocationFormLabel component='legend'>
          <FormattedMessage id='goodeloForm.locationFieldLabel' />
        </LocationFormLabel>
        <StyledFormGroup>
          <Box display='flex' height='3.5rem'>
            <LocationTextField
              value={localGoodelo.lat || ''}
              label={intl.formatMessage({ id: 'goodelo.latitude' })}
              required
              fullWidth
              type='number'
              onChange={e => updateField('lat', Number(e.target.value))}
              ref={latRef}
              inputProps={{ step: 'any' }}
            />
            <LocationTextField
              value={localGoodelo.lng || ''}
              label={intl.formatMessage({ id: 'goodelo.longitude' })}
              required
              fullWidth
              type='number'
              onChange={e => updateField('lng', Number(e.target.value))}
              inputProps={{ step: 'any' }}
            />
            <Button
              onClick={() => {
                if (addressRef.current) addressRef.current.focus();
                setIsAddressVisible(true);
              }}
            >
              <FormattedMessage id='goodeloForm.searchByAddressButton' />
            </Button>
          </Box>
          <AddressLine isVisible={isAddressVisible}>
            {isAddressLoading && <Loader size='2rem' isAbsolute />}
            <AddressTextField
              value={address}
              label={intl.formatMessage({ id: 'goodeloForm.address' })}
              fullWidth
              onChange={e => setAddress(e.target.value)}
              error={!!addressError}
              helperText={addressError}
              onKeyPress={e => {
                if (e.key === 'Enter') {
                  e.stopPropagation();
                  e.preventDefault();
                  if (address) {
                    onAddressSubmit();
                    if (latRef.current) latRef.current.focus();
                  }
                }
              }}
              ref={addressRef}
            />
            <Button
              onClick={() => {
                setIsAddressVisible(false);
                setAddressError('');
              }}
            >
              <FormattedMessage id='cancel' />
            </Button>
            <Button onClick={onAddressSubmit} disabled={!address.trim()}>
              <FormattedMessage id='goodeloForm.searchAddressButton' />
            </Button>
          </AddressLine>
        </StyledFormGroup>
      </FormControl>
      {error && (
        <Box mb={2}>
          <ErrorChip label={error} />
        </Box>
      )}
      {buttons}
      {isLoading && 'Loading...'}
    </form>
  );
};

const LocationFormLabel = styled(FormLabel)`
  margin-bottom: 1rem;
`;

const StyledFormGroup = styled(FormGroup)`
  position: relative;
  margin-bottom: 2rem;
`;

const LocationTextField = styled(TextField)`
  margin-right: 1rem !important;
`;

const AddressLine = styled.div`
  width: 100%;
  height: 6.5rem;
  opacity: ${({ isVisible }) => (isVisible ? 1 : 0)};
  pointer-events: ${({ isVisible }) => (isVisible ? 'all' : 'none')};
  transition: 0.5s all;
  position: absolute;
  top: -1rem;
  right: 0;
  left: 0;
  display: flex;
  background-color: ${({ theme }) => theme.palette.background.paper};
  z-index: 2;
  padding: 1rem 0 2rem 0;
  margin: 0 !important;

  & > button {
    margin-left: 1rem !important;
  }
`;

const AddressTextField = styled(TextField)``;

export default forwardRef(({ ...props }, ref) => (
  <GoodeloForm {...props} formRef={ref} />
));
