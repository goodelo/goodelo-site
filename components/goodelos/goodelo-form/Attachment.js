import React, { useState, useRef } from 'react';
import { useIntl, FormattedMessage } from 'react-intl';
import Box from '@material-ui/core/Box';
import { S3Image } from 'aws-amplify-react';
import styled from '@emotion/styled';

import { MAX_ATTACHMENT_SIZE_BYTES } from 'constants/config';
import { s3Upload } from 'utils/aws';

const Attachment = ({ fileKeyList, onChange, onError, identityId }) => {
  const fileInput = useRef();
  const intl = useIntl();
  const [inputKey, setInputKey] = useState(Date.now());
  const [loadedPortion, setLoadedPortion] = useState(0);

  const refreshFileInput = () => {
    setInputKey(Date.now());
    setLoadedPortion(0);
  };

  const onFileInputChange = async () => {
    const file = fileInput.current.files[0];

    if (!file) {
      return;
    }

    if (file && file.size > MAX_ATTACHMENT_SIZE_BYTES) {
      onError(
        intl.formatMessage(
          { id: 'goodeloForm.attachmentSizeValidationError' },
          { size: MAX_ATTACHMENT_SIZE_BYTES / 1000000 }
        )
      );
      refreshFileInput();
      return;
    }

    try {
      const fileKey = await s3Upload(file, { onProgress: setLoadedPortion });

      onChange([...fileKeyList, fileKey]);
    } catch (e) {
      console.error(e);
      onError(intl.formatMessage({ id: 'goodeloForm.imageUploadingError' }));
    } finally {
      refreshFileInput();
    }
  };

  return (
    <Box display='flex' flexDirection='column'>
      {fileKeyList.map(fileKey => (
        <StyledS3Image
          key={fileKey}
          imgKey={fileKey}
          level='protected'
          identityId={identityId}
        />
      ))}

      {!!loadedPortion && (
        <Box mb={2}>
          <FormattedMessage id='goodeloForm.uploading' />{' '}
          {intl.formatNumber(loadedPortion, { style: 'percent' })}
        </Box>
      )}
      <input
        type='file'
        key={inputKey}
        ref={fileInput}
        onChange={onFileInputChange}
      />
    </Box>
  );
};

const StyledS3Image = styled(S3Image)`
  max-width: 100%;
  max-height: 10rem;
`;

export default Attachment;
