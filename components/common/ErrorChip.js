import React from 'react';
import styled from '@emotion/styled';
import Chip from '@material-ui/core/Chip';
import ErrorIcon from '@material-ui/icons/ErrorOutline';
import { useIntl } from 'react-intl';

const ErrorChip = ({ label }) => {
  const intl = useIntl();

  return (
    <StyledChip
      icon={<ErrorIcon />}
      label={label || intl.formatMessage({ id: 'errorOccurred' })}
      color='primary'
    />
  );
};

const StyledChip = styled(Chip)`
  background-color: ${({ theme }) => theme.palette.error.main} !important;
`;

export default ErrorChip;
