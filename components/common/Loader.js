import React from 'react';
import LoopIcon from '@material-ui/icons/Loop';
import { keyframes } from '@emotion/core';
import styled from '@emotion/styled';

const Loader = ({ size, isAbsolute }) => (
  <Container
    onClick={e => {
      e.stopPropagation();
    }}
    size={size}
    isAbsolute={isAbsolute}
  >
    <LoopIcon />
  </Container>
);

const rotate = keyframes`
  to {
    transform: rotate(-360deg);
  }
`;

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: ${({ isAbsolute }) => (isAbsolute ? 'absolute' : 'fixed')};
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.15);
  z-index: 999;

  & > * {
    font-size: ${({ size }) => (size ? size : '10rem')} !important;
    animation: ${rotate} 2s linear infinite;
  }
`;

export default Loader;
