import React from 'react';
import styled from '@emotion/styled';
import Chip from '@material-ui/core/Chip';
import SuccessIcon from '@material-ui/icons/Done';
import { useIntl } from 'react-intl';

const SuccessChip = ({ label }) => {
  const intl = useIntl();

  return (
    <StyledChip
      icon={<SuccessIcon />}
      label={label || intl.formatMessage({ id: 'success' })}
      color='primary'
    />
  );
};

const StyledChip = styled(Chip)`
  background-color: ${({ theme }) => theme.palette.success.main} !important;
`;

export default SuccessChip;
