import styled from '@emotion/styled';
import Button from 'components/common/Button';

export const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  margin-top: 1rem;

  & > * {
    margin-bottom: 1rem !important;
  }
`;

export const SubmitButton = styled(Button)`
  font-size: ${({ theme }) => theme.typography.h4.fontSize} !important;
  padding: 0.5rem 0 !important;
  width: 100%;
`;
