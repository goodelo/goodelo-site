import React, { forwardRef } from 'react';
import { default as MuiTextField } from '@material-ui/core/TextField';

const TextField = forwardRef(({ ...props }, ref) => (
  <MuiTextField variant='outlined' {...props} inputRef={ref} />
));

export default TextField;
