import React, { useRef, useEffect } from 'react';
import { default as MuiDialog } from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { always } from 'ramda';
import { useIntl } from 'react-intl';

import Button from 'components/common/Button';

const Dialog = ({
  isOpen,
  scroll = 'body',
  onClose,
  title,
  children,
  submitLabel,
  cancelLabel,
  onSubmit,
  onCancel = always(),
  isSubmitDisabled,
  noButtons,
  className,
  maxWidth,
}) => {
  const intl = useIntl();
  const contentElementRef = useRef(null);

  useEffect(() => {
    if (isOpen) {
      const { current: descriptionElement } = contentElementRef;

      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [isOpen]);

  return (
    <MuiDialog
      open={isOpen}
      onClose={onClose}
      scroll={scroll}
      aria-labelledby='scroll-dialog-title'
      aria-describedby='scroll-dialog-description'
      className={className}
      maxWidth={maxWidth}
    >
      {title && <DialogTitle id='scroll-dialog-title'>{title}</DialogTitle>}
      <DialogContent ref={contentElementRef}>{children}</DialogContent>
      {!noButtons && (
        <DialogActions>
          <Button onClick={onCancel} color='primary'>
            {cancelLabel || intl.formatMessage({ id: 'cancel' })}
          </Button>
          <Button
            onClick={onSubmit}
            color='primary'
            disabled={isSubmitDisabled}
          >
            {submitLabel}
          </Button>
        </DialogActions>
      )}
    </MuiDialog>
  );
};

export default Dialog;
