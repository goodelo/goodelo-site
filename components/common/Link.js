import React from 'react';
import { default as RouterLink } from 'next/link';
import styled from '@emotion/styled';
import ButtonBase from '@material-ui/core/ButtonBase';
import Typography from '@material-ui/core/Typography';

const Link = ({
  children,
  textComponent = 'span',
  textVariant = 'subtitle1',
  className,
  ...props
}) => (
  <RouterLink {...props}>
    <StyledButtonBase centerRipple component='span' className={className}>
      <Typography
        variant={textVariant}
        component={textComponent}
        className='link-text'
      >
        {children}
      </Typography>
    </StyledButtonBase>
  </RouterLink>
);

const StyledButtonBase = styled(ButtonBase)`
  width: 100%;
  height: 100%;

  & > span {
    line-height: 0;
  }
`;

export default Link;
