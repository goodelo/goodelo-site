import React, { useEffect, useRef } from 'react';
import { always, sum, prop, pipe, map, divide, __ } from 'ramda';

import {
  MAP_QUEST_API_KEY,
  DEFAULT_MAP_CENTER_LAT,
  DEFAULT_MAP_CENTER_LNG,
  DEFAULT_MAP_ZOOM,
} from 'constants/config';

const MAP_CONTAINER_ID = 'embed-map';

const Map = ({ items, onItemClick = always(), className }) => {
  const isMounted = useRef(false);
  const mapRef = useRef(null);
  const markerLayerRef = useRef(null);

  useEffect(
    function init() {
      if (!isMounted.current) {
        isMounted.current = true;
        const mapRefs = initMap({ items });

        mapRef.current = mapRefs.map;
        markerLayerRef.current = mapRefs.markerLayer;
      }
    },
    [items]
  );

  useEffect(
    function changeMarkers() {
      drawMarkers({
        items,
        markerLayer: markerLayerRef.current,
        onItemClick,
      });
    },
    [items, onItemClick]
  );

  return <div id={MAP_CONTAINER_ID} className={className} />;
};

const initMap = ({ items }) => {
  L.mapquest.key = MAP_QUEST_API_KEY;
  const baseLayer = L.mapquest.tileLayer('map');

  const mapInstance = L.mapquest.map(MAP_CONTAINER_ID, {
    center: L.latLng(
      items.length
        ? pipe(map(prop('lat')), sum, divide(__, items.length))(items)
        : DEFAULT_MAP_CENTER_LAT,
      items.length
        ? pipe(map(prop('lng')), sum, divide(__, items.length))(items)
        : DEFAULT_MAP_CENTER_LNG
    ),
    layers: baseLayer,
    zoom: DEFAULT_MAP_ZOOM,
  });

  const markerLayer = L.markerClusterGroup();

  mapInstance.addLayer(markerLayer);

  return { map: mapInstance, markerLayer };
};

const drawMarkers = ({ items, markerLayer, onItemClick }) => {
  markerLayer.clearLayers();

  items.forEach(item => {
    const marker = L.marker(new L.LatLng(item.lat, item.lng), {
      icon: L.mapquest.icons.marker(),
    });

    marker.on('click', () => onItemClick(item));
    markerLayer.addLayer(marker);
  });
};

export default Map;
