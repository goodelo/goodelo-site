import React from 'react';
import styled from '@emotion/styled';
import ButtonBase from '@material-ui/core/ButtonBase';
import { default as MuiButton } from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const Button = ({
  onClick,
  children,
  variant = 'default',
  className,
  type = 'button',
  disabled,
}) =>
  ({
    default: () => (
      <StyledMuiButton
        onClick={onClick}
        className={className}
        variant='contained'
        color='primary'
        type={type}
        disabled={disabled}
      >
        {children}
      </StyledMuiButton>
    ),
    text: () => (
      <StyledButtonBase
        onClick={onClick}
        className={className}
        type={type}
        centerRipple
        disabled={disabled}
      >
        <Typography variant='subtitle1' component='span'>
          {children}
        </Typography>
      </StyledButtonBase>
    ),
  }[variant]());

const StyledButtonBase = styled(ButtonBase)``;

const StyledMuiButton = styled(MuiButton)`
  .MuiButton-label {
    text-transform: none;
  }
`;

export default Button;
