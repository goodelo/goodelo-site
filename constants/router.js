const MAIN = '/test';

export const ROUTES = {
  TEMP_LANDING: '/',
  MAIN,
  LOGIN: `${MAIN}/login`,
  REGISTER: `${MAIN}/register`,
  RESET_PASSWORD: `${MAIN}/reset-password`,
  MAP: `${MAIN}/map`,
  GOODELO: `${MAIN}/goodelo`,
  USER: `${MAIN}/u`,
  ADD_GOODELO: `${MAIN}/u/add`,
};
