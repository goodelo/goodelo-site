export const GOODELO_CORE_URL = process.env.REACT_APP_BASE_URL;

export const GTAG_ID = process.env.GTAG_ID;
export const PUBLIC_URL = 'https://goodelo.com';
export const IS_PRODUCTION = process.env.IS_PRODUCTION;
export const IS_DEVELOPMENT = process.env.IS_DEVELOPMENT;
export const IS_SERVER_SIDE = typeof window === 'undefined';

const identityPoolId = process.env.REACT_APP_IDENTITY_POOL_ID;

export const AMPLIFY_CONFIG = {
  Auth: {
    region: 'eu-central-1',
    mandatorySignIn: false,
    userPoolId: process.env.REACT_APP_USER_POOL_ID,
    userPoolWebClientId: process.env.REACT_APP_USER_POOL_WEB_CLIENT_ID,
    identityPoolId,
  },
  API: {
    endpoints: [
      {
        name: 'api',
        endpoint: GOODELO_CORE_URL,
      },
    ],
  },
  Storage: {
    AWSS3: {
      bucket: process.env.REACT_APP_S3_BUCKET,
      region: 'eu-central-1',
      identityPoolId,
    },
  },
};

export const MAX_ATTACHMENT_SIZE_BYTES = 1000 * 1000 * 50;

export const MAP_QUEST_API_KEY = process.env.REACT_APP_MAP_QUEST_API_KEY;
export const MAP_QUEST_ADDRESS_GEOCODING_URL = `https://open.mapquestapi.com/geocoding/v1/address`;
export const DEFAULT_MAP_CENTER_LAT = 37.7749;
export const DEFAULT_MAP_CENTER_LNG = -122.4194;
export const DEFAULT_MAP_ZOOM = 3;
