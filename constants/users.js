export const ROLE = {
  ADMIN: 'admin',
  MODERATOR: 'moderator',
  REGULAR: 'regular',
  ANONIM: 'anonim',
};

export const DEFAULT_USER = {
  __typename: 'User',
  username: null,
  email: null,
  role: ROLE.ANONIM,
};
