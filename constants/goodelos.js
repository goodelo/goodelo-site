export const DEFAULT_GOODELO = {
  title: '',
  description: '',
  attachment: [],
};

export const UPDATABLE_FIELDS = [
  'title',
  'description',
  'attachment',
  'lat',
  'lng',
];
