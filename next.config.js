const withImages = require('next-images');

module.exports = withImages({
  env: {
    REACT_APP_BASE_URL: process.env.REACT_APP_BASE_URL,
    REACT_APP_IDENTITY_POOL_ID: process.env.REACT_APP_IDENTITY_POOL_ID,
    REACT_APP_USER_POOL_ID: process.env.REACT_APP_USER_POOL_ID,
    REACT_APP_USER_POOL_WEB_CLIENT_ID:
      process.env.REACT_APP_USER_POOL_WEB_CLIENT_ID,
    REACT_APP_S3_BUCKET: process.env.REACT_APP_S3_BUCKET,
    REACT_APP_MAP_QUEST_API_KEY: process.env.REACT_APP_MAP_QUEST_API_KEY,
  },

  webpack(config, _options) {
    return config;
  },
});
