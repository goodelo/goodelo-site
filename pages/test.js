import React, { useMemo } from 'react';
import { useIntl, FormattedMessage } from 'react-intl';
import { omit, take, pipe, sort, path, pathOr } from 'ramda';
import Typography from '@material-ui/core/Typography';
import { useQuery } from '@apollo/client';

import RegularLayout from 'components/layout/RegularLayout';
import GoodeloList from 'components/goodelos/GoodeloList';
import Button from 'components/common/Button';
import { GET_GOODELOS, GET_MORE_GOODELOS } from 'state/goodelos';
import { apolloClient } from 'state/store';
import { IS_SERVER_SIDE } from 'constants/config';

const Goodelos = ({ featuredGoodelos }) => {
  const intl = useIntl();

  const { data, loading, fetchMore } = useQuery(GET_GOODELOS, {
    skip: IS_SERVER_SIDE,
    notifyOnNetworkStatusChange: true,
  });
  const fetchedGoodelos = useMemo(
    () => pathOr([], ['goodelos', 'items'], data),
    [data]
  );
  const nextPageCursor = path(['goodelos', 'nextPageCursor'], data);

  return (
    <RegularLayout title={intl.formatMessage({ id: 'main.pageTitle' })}>
      <Typography variant='h4'>
        <FormattedMessage id='main.featuredTitle' />
      </Typography>
      <GoodeloList goodelos={featuredGoodelos || []} isPreview />
      <Typography variant='h4'>
        <FormattedMessage id='main.feedTitle' />
      </Typography>
      <GoodeloList goodelos={fetchedGoodelos} isPreview />
      {!fetchedGoodelos.length ? (
        <FormattedMessage id='loading' />
      ) : (
        nextPageCursor && (
          <Button
            disabled={loading}
            onClick={() =>
              fetchMore({
                query: GET_MORE_GOODELOS,
                variables: { cursor: omit(['__typename'], nextPageCursor) },
                updateQuery: (previousResult, { fetchMoreResult }) => ({
                  goodelos: {
                    items: [
                      ...previousResult.goodelos.items,
                      ...fetchMoreResult.goodelos.items,
                    ],
                    nextPageCursor: fetchMoreResult.goodelos.nextPageCursor,
                    __typename: previousResult.goodelos.__typename,
                  },
                }),
              })
            }
          >
            <Typography variant='h4' component='span'>
              <FormattedMessage id='main.more' />
            </Typography>
          </Button>
        )
      )}
    </RegularLayout>
  );
};

export async function getStaticProps() {
  const { data } = await apolloClient.query({
    query: GET_GOODELOS,
  });

  return {
    props: {
      featuredGoodelos: pipe(
        sort(
          ({ attachment: attachment1Str }, { attachment: attachment2Str }) =>
            attachment2Str.length - attachment1Str.length + Math.random() - 0.5
        ),
        take(3)
      )(data.goodelos.items),
    },
  };
}

export default Goodelos;
