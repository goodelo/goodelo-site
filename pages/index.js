import React from 'react';
import styled from '@emotion/styled';
import { keyframes } from 'emotion';

import Logo from 'assets/icons/logo.svg';

const Landing = () => {
  return (
    <Container>
      <Header>
        <TitleWrapper>
          <Title>GooDelo</Title>
          <SubTitle>Good Deed Feed</SubTitle>
        </TitleWrapper>
        <ImgWrapper>
          <StyledLogo />
        </ImgWrapper>
      </Header>
      <Headline>
        Volunteer project with open management and global team
      </Headline>
      <Text>
        <P>
          A good deed could bring much more positive impact to the world if word
          about it would be shared with others, because we like to realise that
          we live in a good world.
        </P>
        <P>
          GooDelo project's mission is to help to collect, preserve and share
          information about good deeds made by people, with respect to others
          rights.
        </P>
        <P>
          To see project current status or if you would like to contribute
          please go to{' '}
          <a href='https://trello.com/goodelo'>trello.com/goodelo</a>
        </P>
        <P>
          To send us a good deed, please email it to{' '}
          <a href='mailto:goodelobot+share@gmail.com'>
            goodelobot+share@gmail.com
          </a>{' '}
          (you can attach images or put video link).
        </P>
        <P>Much better ways to add goodelos are on the way!</P>
        <P>All deeds are shown anonymously.</P>
      </Text>
      <svg>
        <defs>
          <linearGradient id='gradient' x1='0%' y1='0%' x2='100%' y2='100%'>
            <stop offset='0%' stopColor='rgba(0,0,0,1)' />
            <stop offset='30%' stopColor='rgba(44,174,79,1)' />
            <stop offset='70%' stopColor='rgba(44,174,79,1)' />
            <stop offset='100%' stopColor='rgba(0,0,0,1)' />
          </linearGradient>
        </defs>
      </svg>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow: auto;
  background-color: rgba(210, 255, 82, 1);
  background: linear-gradient(
    to bottom,
    rgba(210, 255, 82, 1) 0%,
    rgba(42, 161, 73, 1) 100%
  );
`;

const Header = styled.div`
  flex: 0 0 auto;
  display: flex;
  align-items: center;
  justify-content: space-around;
  padding: 10vw 3vw;
  text-shadow: 3px 3px 0 rgba(0, 0, 0, 0.1), -1px -1px 0 rgba(0, 0, 0, 0.1),
    1px -1px 0 rgba(0, 0, 0, 0.1), -1px 1px 0 rgba(0, 0, 0, 0.1),
    1px 1px 0 rgba(0, 0, 0, 0.1);
`;

const TitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const Title = styled.h1`
  margin: 0;
  font-size: 10rem;
`;

const SubTitle = styled.h2`
  margin: 0;
  font-size: 8rem;
`;

const Headline = styled.h3`
  margin: 0;
  padding: 0 5vw;
  text-align: center;
  font-size: 6rem;
  margin-bottom: 5vw;
`;

const Text = styled.div`
  padding: 0 10vw;
  font-size: 5rem;

  a {
    color: black;
  }

  @media (min-width: 800px) {
    font-size: 32px;
  }
`;

const P = styled.p``;

const ImgWrapper = styled.div`
  width: 25vw;
  height: 25vw;
  flex: 0 0 auto;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const pulse = keyframes`
  0%
  {
    transform: scale( .90 );
  }
  10%
  {
    transform: scale( .90 );
  }
  20%
  {
    transform: scale( 1 );
  }
  30%
  {
    transform: scale( .90 );
  }
  40%
  {
    transform: scale( .90 );
  }
  50%
  {
    transform: scale( 1 );
  }
  60%
  {
    transform: scale( .90 );
  }
  80%
  {
    transform: scale( .90 );
  }
  100%
  {
    transform: scale( .90 );
  }
`;

const StyledLogo = styled(Logo)`
  animation: ${pulse} 1s infinite;
  width: 100%;

  path {
    stroke: url(#gradient) !important;
  }
`;

export default Landing;
