import React, { useState } from 'react';
import Box from '@material-ui/core/Box';
import { useRouter } from 'next/router';
import { path } from 'ramda';
import Head from 'next/head';
import { useIntl, FormattedMessage } from 'react-intl';

import { ROUTES } from 'constants/router';

import RegularLayout from 'components/layout/RegularLayout';
import Link from 'components/common/Link';
import TextField from 'components/common/TextField';
import ErrorChip from 'components/common/ErrorChip';
import Loader from 'components/common/Loader';
import { StyledForm, SubmitButton } from 'components/common/styled';

import { signIn } from 'utils/auth';
import { currentUserVar } from 'state/users';
import { ROLE } from 'constants/users';

function Login() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();
  const intl = useIntl();

  const onSubmit = async e => {
    e.preventDefault();
    setError('');

    try {
      setIsLoading(true);
      const result = await signIn(username, password);

      const attributes =
        path(['attributes'], result) ||
        path(['challengeParam', 'userAttributes'], result);

      currentUserVar({
        __typename: 'User',
        username: result.username,
        email: attributes['email'],
        role: attributes['custom:role'] || ROLE.REGULAR,
      });

      router.push(ROUTES.MAIN);
    } catch (e) {
      setError(e.message);
      setIsLoading(false);
      console.error(e);
    }
  };

  return (
    <RegularLayout title={intl.formatMessage({ id: 'login.pageTitle' })}>
      <Head>
        <title>{intl.formatMessage({ id: 'login.pageSEOTitle' })}</title>
        <meta
          name='description'
          content={intl.formatMessage({ id: 'login.pageSEODescription' })}
        />
      </Head>
      {isLoading && <Loader />}
      <StyledForm onSubmit={onSubmit}>
        <TextField
          required
          fullWidth
          inputProps={{
            autoComplete: 'username',
          }}
          label={intl.formatMessage({ id: 'login.username' })}
          value={username}
          onChange={e => setUsername(e.target.value)}
        />
        <TextField
          required
          fullWidth
          inputProps={{
            autoComplete: 'current-password',
          }}
          type='password'
          label={intl.formatMessage({ id: 'login.password' })}
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
        <Box display='flex' justifyContent='center'>
          <SubmitButton type='submit' id='login-submit'>
            <FormattedMessage id='login.submitLabel' />
          </SubmitButton>
        </Box>
        <Box display='flex' justifyContent='center'>
          {error && <ErrorChip label={error} />}
        </Box>
        <Box mb='1rem'>
          <Link href={ROUTES.REGISTER}>
            <FormattedMessage id='login.registerLink' />
          </Link>
        </Box>
        <Link href={ROUTES.RESET_PASSWORD}>
          <FormattedMessage id='login.resetPasswordLink' />
        </Link>
      </StyledForm>
    </RegularLayout>
  );
}

export default Login;
