import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';
import styled from '@emotion/styled';
import { prop } from 'ramda';

import RegularLayout from 'components/layout/RegularLayout';
import GoodeloCard from 'components/goodelos/GoodeloCard';
import { apolloClient } from 'state/store';
import { GET_GOODELO } from 'state/goodelos';
import Map from 'components/common/Map';

function GoodeloPage({ goodelo }) {
  const router = useRouter();
  const intl = useIntl();

  return (
    <RegularLayout title={intl.formatMessage({ id: 'main.pageTitle' })}>
      {router.isFallback ? (
        <div>Goodelo is loading...</div>
      ) : (
        <>
          {goodelo && (
            <>
              <GoodeloCard
                goodelo={goodelo}
                onGoodeloUpdate={() => {
                  // @todo instead of updating the whole page we need to update only goodelo data on the page
                  router.reload();
                }}
              />
              <StyledMap items={[goodelo]} />
            </>
          )}
          {!goodelo && <div>Goodelo not found</div>}
        </>
      )}
    </RegularLayout>
  );
}

const StyledMap = styled(Map)`
  margin-top: 1rem;
  height: 50vh;
`;

// Not using static page generation because unstable_revalidate feature
// is not working yet, so it is impossible now to refresh static page content
export async function getServerSideProps({ res, params: { id } }) {
  try {
    const { data } = await apolloClient.query({
      query: GET_GOODELO,
      variables: { id },
      fetchPolicy: 'no-cache',
    });

    return {
      props: {
        goodelo: prop('goodelo', data),
      },
    };
  } catch (e) {
    console.error(e);
    return {
      props: {
        goodelo: null,
      },
    };
  }
}

export default GoodeloPage;
