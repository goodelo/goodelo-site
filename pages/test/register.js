import React, { useState, useCallback } from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Head from 'next/head';
import { useIntl, FormattedMessage } from 'react-intl';

import { ROUTES } from 'constants/router';
import { ROLE } from 'constants/users';

import RegularLayout from 'components/layout/RegularLayout';
import Link from 'components/common/Link';
import TextField from 'components/common/TextField';
import ErrorChip from 'components/common/ErrorChip';
import { StyledForm, SubmitButton } from 'components/common/styled';
import Loader from 'components/common/Loader';

import { signUp, confirmSignUp } from 'utils/auth';

function Register() {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [code, setCode] = useState('');
  const [error, setError] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const intl = useIntl();

  const [step, setStep] = useState('register'); // 'register' | 'confirm' | 'success';

  const onSubmit = useCallback(
    async e => {
      e.preventDefault();

      try {
        setIsLoading(true);
        setError('');
        await signUp({
          username,
          password,
          email,
          role: ROLE.REGULAR,
        });

        setStep('confirm');
      } catch (e) {
        setError(e.message);
        console.error(e);
      } finally {
        setIsLoading(false);
      }
    },
    [username, password, email]
  );

  const onConfirmSubmit = useCallback(
    async e => {
      e.preventDefault();

      try {
        setIsLoading(true);
        setError('');
        await confirmSignUp(username, code);
        setStep('success');
      } catch (e) {
        setError(e.message);
        console.error(e);
      } finally {
        setIsLoading(false);
      }
    },
    [username, code]
  );

  return (
    <RegularLayout title={intl.formatMessage({ id: 'register.pageTitle' })}>
      <Head>
        <title>{intl.formatMessage({ id: 'register.pageSEOTitle' })}</title>
        <meta
          name='description'
          content={intl.formatMessage({ id: 'register.pageSEODescription' })}
        />
      </Head>
      {isLoading && <Loader />}
      {step === 'register' && (
        <StyledForm onSubmit={onSubmit}>
          <TextField
            required
            fullWidth
            inputProps={{
              autoComplete: 'username',
            }}
            label={intl.formatMessage({ id: 'register.username' })}
            value={username}
            onChange={e => setUsername(e.target.value)}
          />
          <TextField
            required
            fullWidth
            inputProps={{
              autoComplete: 'email',
            }}
            type='email'
            label={intl.formatMessage({ id: 'register.email' })}
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
          <TextField
            required
            fullWidth
            inputProps={{
              autoComplete: 'new-password',
            }}
            type='password'
            label={intl.formatMessage({ id: 'register.password' })}
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
          <Box display='flex' justifyContent='center'>
            <SubmitButton type='submit'>
              <FormattedMessage id='register.submitLabel' />
            </SubmitButton>
          </Box>
          <Box display='flex' justifyContent='center'>
            {error && <ErrorChip label={error} />}
          </Box>
          <Link href={ROUTES.LOGIN}>
            <FormattedMessage id='register.loginLink' />
          </Link>
        </StyledForm>
      )}
      {step === 'confirm' && (
        <StyledForm onSubmit={onConfirmSubmit}>
          <TextField
            required
            fullWidth
            inputProps={{
              autoComplete: 'one-time-code',
            }}
            label={intl.formatMessage({ id: 'register.confirmCode' })}
            value={code}
            onChange={e => setCode(e.target.value)}
          />
          <Box display='flex'>
            <SubmitButton type='submit'>
              <FormattedMessage id='register.confirmSubmit' />
            </SubmitButton>
            {error && (
              <Box display='flex' justifyContent='center'>
                <ErrorChip label={error} />
              </Box>
            )}
          </Box>
        </StyledForm>
      )}
      {step === 'success' && (
        <Box display='flex' alignItems='center'>
          <Box>
            <Typography variant='subtitle1' component='span'>
              <FormattedMessage id='register.sucess' />
            </Typography>
          </Box>
          <Box mx='1rem'>
            <Typography variant='h5' component='span'>
              👉
            </Typography>
          </Box>
          <Box>
            <Link href={ROUTES.LOGIN}>
              <FormattedMessage id='register.successActionLink' />
            </Link>
          </Box>
        </Box>
      )}
    </RegularLayout>
  );
}

export default Register;
