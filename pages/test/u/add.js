import React, { useState } from 'react';
import { useIntl, FormattedMessage } from 'react-intl';
import { useRouter } from 'next/router';
import { path, pipe, pick, assoc } from 'ramda';

import RegularLayout from 'components/layout/RegularLayout';
import GoodeloForm from 'components/goodelos/goodelo-form/GoodeloForm';
import privateRoute from 'components/privateRoute';
import Button from 'components/common/Button';

import { ROUTES } from 'constants/router';
import { UPDATABLE_FIELDS } from 'constants/goodelos';
import { getCurrentCredentials } from 'utils/auth';
import { validateGoodelo } from 'utils/goodelos';
import { CREATE_GOODELO, GET_GOODELOS } from 'state/goodelos';
import { useAuthMutation } from 'hooks/api';

const AddGoodelo = () => {
  const intl = useIntl();
  const router = useRouter();
  const [error, setError] = useState(null);
  const [isSubmitDisabled, setIsSubmitDisabled] = useState(true);

  const [createGoodelo, { loading }] = useAuthMutation(CREATE_GOODELO, {
    update(cache, mutationResult) {
      const newGoodelo = path(['data', 'createGoodelo'], mutationResult);
      const result = cache.readQuery({ query: GET_GOODELOS });
      const cachedData = path(['goodelos'], result) || {};
      const { items = [], ...rest } = cachedData;

      cache.writeQuery({
        query: GET_GOODELOS,
        data: {
          goodelos: { items: [newGoodelo, ...items], ...rest },
        },
      });
    },
  });

  const onSubmit = async goodeloToSave => {
    setError(null);

    try {
      const creds = await getCurrentCredentials();

      const data = await createGoodelo({
        variables: {
          input: pipe(
            pick(UPDATABLE_FIELDS),
            assoc('identityId', creds.identityId)
          )(goodeloToSave),
        },
      });

      console.log('Goodelo created:', data);
      router.push(ROUTES.MAIN);
    } catch (e) {
      console.error(e);
      setError(intl.formatMessage({ id: 'addGoodelo.saveError' }));
    }

    return false;
  };

  const onLocalUpdate = newGoodelo => {
    setIsSubmitDisabled(!validateGoodelo(newGoodelo));
  };

  return (
    <RegularLayout title={intl.formatMessage({ id: 'addGoodelo.pageTitle' })}>
      <GoodeloForm
        onSubmit={onSubmit}
        isLoading={loading}
        error={error}
        setError={setError}
        onLocalUpdate={onLocalUpdate}
        buttons={
          <Button type='submit' disabled={isSubmitDisabled}>
            <FormattedMessage id='addGoodelo.submit' />
          </Button>
        }
      />
    </RegularLayout>
  );
};

export default privateRoute(AddGoodelo);
