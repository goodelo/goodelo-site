import React from 'react';
import { useIntl, FormattedMessage } from 'react-intl';
import Box from '@material-ui/core/Box';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { pathOr } from 'ramda';

import ChangeUserEmailForm from 'components/users/ChangeUserEmailForm';
import RegularLayout from 'components/layout/RegularLayout';
import GoodeloList from 'components/goodelos/GoodeloList';
import privateRoute from 'components/privateRoute';

import { apolloClient } from 'state/store';

import { GET_USER } from 'state/users';

import { isEmptyUser } from 'utils/users';
import { useCurrentUser } from 'hooks/users';

const Account = ({ user, goodelos }) => {
  const intl = useIntl();
  const currentUser = useCurrentUser();
  const { username, email, role } = user || {};

  return (
    <RegularLayout title={intl.formatMessage({ id: 'account.pageTitle' })}>
      <Box display='flex' flexDirection='column' pb={2}>
        <Box display='flex' alignItems='center' height='2em'>
          <Box mr={1}>
            <Typography variant='body2'>
              <FormattedMessage id='account.username' />:
            </Typography>
          </Box>
          <Typography variant='body1'>{username}</Typography>
        </Box>
        <Box display='flex' alignItems='center' height='2em'>
          <Box mr={1}>
            <Typography variant='body2'>
              <FormattedMessage id='account.email' />:
            </Typography>
          </Box>
          <Typography variant='body1'>{email}</Typography>
        </Box>

        <Box display='flex' alignItems='center' height='2em'>
          <Box mr={1}>
            <Typography variant='body2'>
              <FormattedMessage id='account.role' />:
            </Typography>
          </Box>
          <Typography variant='body1'>{role}</Typography>
        </Box>
      </Box>

      <Box>
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Box display='flex' alignItems='center'>
              <Typography variant='subtitle1'>
                <FormattedMessage
                  id='account.userGoodelosTitle'
                  values={{ number: (goodelos || []).length }}
                />
              </Typography>
            </Box>
          </ExpansionPanelSummary>

          <ExpansionPanelDetails>
            <GoodeloList goodelos={goodelos || []} isPreview />
          </ExpansionPanelDetails>
        </ExpansionPanel>

        {!isEmptyUser(currentUser) && currentUser.username === username && (
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography variant='subtitle1'>
                <FormattedMessage id='account.changeEmailTitle' />
              </Typography>
            </ExpansionPanelSummary>

            <ExpansionPanelDetails>
              <ChangeUserEmailForm />
            </ExpansionPanelDetails>
          </ExpansionPanel>
        )}
      </Box>
    </RegularLayout>
  );
};

export async function getServerSideProps({ res, params: { username } }) {
  try {
    const { data } = await apolloClient.query({
      query: GET_USER,
      variables: { username },
    });

    return {
      props: {
        user: pathOr(null, ['user', 'user'], data),
        goodelos: pathOr([], ['user', 'goodelos'], data),
      },
    };
  } catch (e) {
    console.error(JSON.stringify(e));
    return {
      props: {
        user: null,
        goodelos: [],
      },
    };
  }
}

export default privateRoute(Account);
