import React, { useState, useCallback } from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Head from 'next/head';
import { useIntl, FormattedMessage } from 'react-intl';

import { ROUTES } from 'constants/router';

import RegularLayout from 'components/layout/RegularLayout';
import Link from 'components/common/Link';
import TextField from 'components/common/TextField';
import ErrorChip from 'components/common/ErrorChip';
import { StyledForm, SubmitButton } from 'components/common/styled';
import Loader from 'components/common/Loader';

import { forgotPassword, forgotPasswordSubmit } from 'utils/auth';

function ResetPassword() {
  const [username, setUsername] = useState('');
  const [code, setCode] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const intl = useIntl();

  const [step, setStep] = useState('reset'); // 'reset' | 'confirm' | 'success';

  const onSubmit = useCallback(
    async e => {
      e.preventDefault();

      try {
        setIsLoading(true);
        setError('');
        await forgotPassword(username);

        setStep('confirm');
      } catch (e) {
        setError(e.message);
        console.error(e);
      } finally {
        setIsLoading(false);
      }
    },
    [username]
  );

  const onConfirmSubmit = useCallback(
    async e => {
      e.preventDefault();

      try {
        setIsLoading(true);
        setError('');
        await forgotPasswordSubmit(username, code, password);
        setStep('success');
      } catch (e) {
        setError(e.message);
        console.error(e);
      } finally {
        setIsLoading(false);
      }
    },
    [username, password, code]
  );

  return (
    <RegularLayout
      title={intl.formatMessage({ id: 'resetPassword.pageTitle' })}
    >
      <Head>
        <title>
          {intl.formatMessage({ id: 'resetPassword.pageSEOTitle' })}
        </title>
        <meta
          name='description'
          content={intl.formatMessage({
            id: 'resetPassword.pageSEODescription',
          })}
        />
      </Head>
      {isLoading && <Loader />}
      {step === 'reset' && (
        <StyledForm onSubmit={onSubmit}>
          <TextField
            required
            fullWidth
            inputProps={{
              autoComplete: 'username',
            }}
            label={intl.formatMessage({ id: 'resetPassword.username' })}
            value={username}
            onChange={e => setUsername(e.target.value)}
          />
          <Box display='flex' justifyContent='center'>
            <SubmitButton type='submit'>
              <FormattedMessage id='resetPassword.sendCodeSubmit' />
            </SubmitButton>
          </Box>
          <Box display='flex' justifyContent='center'>
            {error && <ErrorChip label={error} />}
          </Box>
          <Link href={ROUTES.LOGIN}>
            <FormattedMessage id='resetPassword.loginLink' />
          </Link>
        </StyledForm>
      )}
      {step === 'confirm' && (
        <StyledForm onSubmit={onConfirmSubmit}>
          <TextField
            required
            fullWidth
            inputProps={{
              autoComplete: 'one-time-code',
            }}
            label={intl.formatMessage({ id: 'resetPassword.confirmCode' })}
            value={code}
            onChange={e => setCode(e.target.value)}
          />
          <TextField
            required
            fullWidth
            inputProps={{
              autoComplete: 'new-password',
            }}
            type='password'
            label={intl.formatMessage({ id: 'resetPassword.newPassword' })}
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
          <Box display='flex'>
            <SubmitButton type='submit'>
              <FormattedMessage id='resetPassword.submitLabel' />
            </SubmitButton>
            {error && (
              <Box display='flex' justifyContent='center'>
                <ErrorChip label={error} />
              </Box>
            )}
          </Box>
        </StyledForm>
      )}
      {step === 'success' && (
        <Box display='flex' alignItems='center'>
          <Box>
            <Typography variant='subtitle1' component='span'>
              <FormattedMessage id='resetPassword.sucess' />
            </Typography>
          </Box>
          <Box mx='1rem'>
            <Typography variant='h5' component='span'>
              👉
            </Typography>
          </Box>
          <Box>
            <Link href={ROUTES.LOGIN}>
              <FormattedMessage id='resetPassword.successActionLink' />
            </Link>
          </Box>
        </Box>
      )}
    </RegularLayout>
  );
}

export default ResetPassword;
