import React, { useMemo, useState } from 'react';
import { useIntl, FormattedMessage } from 'react-intl';
import { pipe, pathOr, filter, prop } from 'ramda';
import { useQuery } from '@apollo/client';
import styled from '@emotion/styled';

import FullPageLayout from 'components/layout/FullPageLayout';
import GoodeloCard from 'components/goodelos/GoodeloCard';
import { GET_MAP_GOODELOS } from 'state/goodelos';
import { IS_SERVER_SIDE } from 'constants/config';
import Map from 'components/common/Map';
import Dialog from 'components/common/Dialog';

const GoodeloMap = () => {
  const intl = useIntl();
  const [clickedGoodelo, setClickedGoodelo] = useState(null);

  const { data } = useQuery(GET_MAP_GOODELOS, { skip: IS_SERVER_SIDE });

  const fetchedGoodelos = useMemo(
    () => pipe(pathOr([], ['goodelosAll', 'items']), filter(prop('lat')))(data),
    [data]
  );

  return (
    <FullPageLayout title={intl.formatMessage({ id: 'map.title' })}>
      <Dialog
        isOpen={!!clickedGoodelo}
        onClose={() => setClickedGoodelo(null)}
        noButtons
        maxWidth={false}
      >
        {clickedGoodelo && (
          <GoodeloCard
            goodelo={clickedGoodelo}
            isPreview
            onGoodeloUpdate={setClickedGoodelo}
          />
        )}
      </Dialog>

      {!fetchedGoodelos.length ? (
        <FormattedMessage id='loading' />
      ) : (
        <StyledMap items={fetchedGoodelos} onItemClick={setClickedGoodelo} />
      )}
    </FullPageLayout>
  );
};

const StyledMap = styled(Map)`
  flex: 1;
`;

export default GoodeloMap;
