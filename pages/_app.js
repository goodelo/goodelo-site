import { useEffect } from 'react';
import Head from 'next/head';
import { IntlProvider } from 'react-intl';
import Amplify from 'aws-amplify';
import dynamic from 'next/dynamic';

import '@aws-amplify/ui/dist/style.css';
import 'nprogress/nprogress.css';

import 'styles/global.css';
import {
  GTAG_ID,
  PUBLIC_URL,
  IS_PRODUCTION,
  AMPLIFY_CONFIG,
} from 'constants/config';

import ThemeProvider from 'components/ThemeProvider';
import translations from 'translations/en.json';
import { withStore } from 'state/store';

Amplify.configure(AMPLIFY_CONFIG);

const TopProgressBar = dynamic(
  () => import('components/common/TopProgressBar'),
  { ssr: false }
);

function App({ Component, pageProps, router }) {
  useEffect(function init() {
    // Remove the server-side injected CSS of material-ui.
    const jssStyles = document.querySelector('#jss-server-side');

    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }

    if (IS_PRODUCTION) {
      window.dataLayer = window.dataLayer || [];
      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());

      gtag('config', GTAG_ID);
    }
  }, []);

  return (
    <IntlProvider locale='en' messages={translations}>
      <ThemeProvider>
        <Head>
          <title key='title'>{META_INFO.title}</title>

          <meta name='description' content={META_INFO.description} />
          <meta name='keywords' content={META_INFO.keywords} />
          <link rel='manifest' href='/manifest.json' />
          <meta name='viewport' content='width=device-width, initial-scale=1' />
          <meta name='theme-color' content='#000000' />

          <meta property='og:title' content={META_INFO.title} />
          <meta property='og:description' content={META_INFO.description} />
          <meta property='og:type' content={META_INFO.type} />
          <meta property='og:image' content={META_INFO.image} />
          <meta property='og:url' content={META_INFO.url} />

          <link
            rel='apple-touch-icon'
            sizes='180x180'
            href='/apple-touch-icon.png'
          />
          <link
            rel='icon'
            type='image/png'
            sizes='32x32'
            href='/favicon-32x32.png'
          />
          <link
            rel='icon'
            type='image/png'
            sizes='16x16'
            href='/favicon-16x16.png'
          />
          <link rel='mask-icon' href='/safari-pinned-tab.svg' color='#2daf51' />
          <meta name='msapplication-TileColor' content='#2b5797' />

          {IS_PRODUCTION ? (
            <script
              async
              src={`https://www.googletagmanager.com/gtag/js?id=${GTAG_ID}`}
            ></script>
          ) : (
            <meta name='robots' content='noindex' />
          )}

          <script src='https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest.js'></script>
          <script src='https://unpkg.com/leaflet.markercluster@1.0.6/dist/leaflet.markercluster.js'></script>
          <link
            type='text/css'
            rel='stylesheet'
            href='https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest.css'
          />
          <link
            type='text/css'
            rel='stylesheet'
            href='https://unpkg.com/leaflet.markercluster@1.0.6/dist/MarkerCluster.css'
          />
          <link
            type='text/css'
            rel='stylesheet'
            href='https://unpkg.com/leaflet.markercluster@1.0.6/dist/MarkerCluster.Default.css'
          />
        </Head>

        <TopProgressBar />
        <Component {...pageProps} />
      </ThemeProvider>
    </IntlProvider>
  );
}

const META_INFO = {
  title: 'GooDelo - share GooD',
  description:
    'Open project GooDelo - Good Deed Feed. Volunteer project with open management and global team',
  keywords:
    'frontend, developer, consultant, react, web development, web developer',
  image: `${PUBLIC_URL}/android-chrome-512x512.png`,
  url: PUBLIC_URL,
  type: 'website',
};

export default withStore(App);
